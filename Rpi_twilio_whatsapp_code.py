# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client #You must install this Library in Your System.
import RPi.GPIO as GPIO


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
# You will Find SID and TOKEN on Twilio's Dashboard page. Just copy and paste them.
account_sid = 'Paste Your SID Here'
auth_token = 'Paste Your Token Here'
client = Client(account_sid, auth_token)
count=0                 # This variable control the Flow of message sending.Without this messages will be send more than for each State(Low Or High).
msg=0                   # Stores the Message(User Defined).

PIR_input = 29				#read PIR input.Low or High
# Led is Optional.If you want, you can connect.
LED = 32				#LED 
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)		#choose pin number system 
GPIO.setup(PIR_input, GPIO.IN)          #PIR_input as Input.
GPIO.setup(LED, GPIO.OUT)               #LED as an OUTPUT.
GPIO.output(LED, GPIO.LOW)              #Initially Led must be off.
def send(msg):
    message = client.messages \
        .create(
             from_='whatsapp:+14xxxxxxxx',  # Put your twilio's whatsapp number here.You will get it from Twilio's Whatsapp beta section.
             body=msg,                      # message is assigned to body of Twlio's API.
             to='whatsapp:+9185xxxxxx'      # Put your mobile number here starting with your country code e.g. +918xxxxxxxxx
         )

    print(message.sid)

while 1:
    if(GPIO.input(PIR_input)):
        msg="*Alert!!! Somebody At Home*" # Assigning this value to "msg" variable when PIR is HIGH.
        GPIO.output(LED, GPIO.HIGH)
        if(count==0):
            send(msg)
            count=1
            
    else:
        GPIO.output(LED, GPIO.LOW)
        if(count==1):
            count=0
        
